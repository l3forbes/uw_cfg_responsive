<?php
/**
 * @file
 * uw_cfg_responsive.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_cfg_responsive_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'responsive_menu';
  $context->description = 'Adds responsive menu';
  $context->tag = 'Navigation';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'responsive_menu_combined-responsive_menu_combined' => array(
          'module' => 'responsive_menu_combined',
          'delta' => 'responsive_menu_combined',
          'region' => 'site_header',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds responsive menu');
  t('Navigation');
  $export['responsive_menu'] = $context;

  return $export;
}
